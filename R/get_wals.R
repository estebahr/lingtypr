#' @title get_wals
#' @description extracts the WALS values of a given language-feature combination
#' @param language A vector of language names (as used in WALS). Defaults to NULL.
#' @param glottocode A vector of glottocodes. Defaults to NULL.
#' @param feature A vector of feature IDs (as used in WALS). Defaults to NULL.
#' @return Returns a dataframe with the WALS information for the selected language-feature pair(s).
#' @examples
#' mylanguages <- c("Aari", "Korku")
#' myfeatures <- c("1A", "69A")
#' myvalues <- get_wals(language = mylanguages, feature = myfeatures)
#' @export
#' @importFrom dplyr "%>%"
get_wals <- function(language=NULL, glottocode=NULL, feature=NULL){

    ## language
    if(!is.null(language) & is.null(feature) & is.null(glottocode)){

        out <- lapply(language, function(x){
            helper <- wals[wals$language==x, ]

            ## warning for nonexisting language
            if(nrow(wals[wals$language==x, ])==0){
                stop(paste("the language",
                           paste("'", x, "'", sep = ""), "is not found in WALS", sep = " "))
            }

            else{
                helper
            }


        })

        out <- do.call("rbind", out)
        out
            
    }

    ## glottocode
    else if(!is.null(glottocode) & is.null(feature) & is.null(language)){

        out <- lapply(glottocode, function(x){
            helper <- wals[wals$glottocode==x & !is.na(wals$glottocode), ]

            ## warning for nonexisting glottocode
            if(nrow(wals[wals$glottocode==x, ])==0){
                stop(paste("the glottocode",
                           paste("'", x, "'", sep = ""), "is not found in WALS", sep = " "))
            }

            else{
                helper
            }

        })

        out <- do.call("rbind", out)
        out

    }

    ## feature
    else if(!is.null(feature) & is.null(language) & is.null(glottocode)){

        out <- lapply(feature, function(x){
            helper <- wals[wals$feature_id==x, ]

            ## warning for nonexisting feature
            if(nrow(wals[wals$feature_id==x, ])==0){                
                stop(paste("the feature",
                           paste("'", x, "'", sep = ""), "is not found in WALS", sep = " "))
            }

            else{
                helper
            }

            
        })

        out <- do.call("rbind",out)
        out

    }
    
    ## language + feature
    else if(!is.null(language) & !is.null(feature) & is.null(glottocode)){

        ## check for nonexisting language
        for(i in language){

            if(isFALSE(i %in% wals$language)){
                stop(paste("the language",
                           paste("'", i, "'", sep = ""), "is not found in WALS", sep = " "))
            }
            
        }

        ## check for nonexisting feature
        for(i in feature){

            if(isFALSE(i %in% wals$feature_id)){
                stop(paste("the feature",
                           paste("'", i, "'", sep = ""), "is not found in WALS", sep = " "))
            }
            
        }

        ## ## warning for nonexisting language
        ## if(nrow(wals[wals$language==language, ])==0){
        ##     stop(paste("the language", x, "could not be found", sep = " "))
        ## }
        ## warning for nonexisting feature
        ## else if(nrow(wals[wals$feature_id==x, ])==0){
        ##     stop(paste("the feature", x, "could not be found", sep = " "))
        ## }

        ## else{

        helper1 <- wals[wals$language %in% language, "wals_code"] 
        helper1 <- unique(helper1)


        id <- as.vector(outer(feature, helper1, paste, sep="-"))
        
        out <- lapply(id, function(x){
            helper <- wals[wals$ID==x, ]
            helper


        })

        out <- do.call("rbind", out)
        
        if(nrow(out)==0){
            warning("no entry was found for this language-feature combination")
        }
        else{
            out
        }

        ## }

    }

    ## glottocode + feature
    else if(!is.null(glottocode) & !is.null(feature) & is.null(language)){

        ## check for nonexisting glottocode
        for(i in glottocode){

            if(isFALSE(i %in% wals$glottocode)){
                stop(paste("the glottocode",
                           paste("'", i, "'", sep = ""), "is not found in WALS", sep = " "))
            }
            
        }

        ## check for nonexisting feature
        for(i in feature){

            if(isFALSE(i %in% wals$feature_id)){
                stop(paste("the feature",
                           paste("'", i, "'", sep = ""), "is not found in WALS", sep = " "))
            }
            
        }

        helper1 <- wals[wals$glottocode %in% glottocode, "wals_code"] 
        helper1 <- unique(helper1)

        id <- as.vector(outer(feature, helper1, paste, sep="-"))
        
        out <- lapply(id, function(x){
            helper <- wals[wals$ID==x, ]
            helper
            
            ## ## warning for nonexisting glottocode
            ## if(nrow(wals[wals$glottocode==x, ])==0){
            ##     stop(paste("the glottocode", x, "could not be found", sep = " "))
            ## }
            ## ## warning for nonexisting feature
            ## else if(nrow(wals[wals$feature_id==x, ])==0){
            ##     stop(paste("the feature", x, "could not be found", sep = " "))
            ## }

            ## else{
            ##     helper
            ## }
            
        })

        out <- do.call("rbind", out)
        
        if(nrow(out)==0){
            warning("no entry was found for this glottocode-feature combination")
        }

        else{
            out
        }

    }

    ## NULL

    else if(is.null(feature) & is.null(language) & is.null(glottocode)){
        stop("please specify at least a language, glottocode, or feature")
    }

    ## glottolog + language
    else if(!is.null(language) & !is.null(glottocode)){
        stop("please provide either languages or glottocodes, not both")
    }

}


