#' @title add_glottolog
#' @description Adds glottolog information to the dataset by language name or glottocode.
#' @param data a dataframe containing either a column with languages (needs to be named "language") or glottocode (needs to be named "glottocode").
#' @param by Option to match by language names ('language') or glottocodes ('glottocode'). Defaults to 'language'.
#' @return Returns a dataframe with added glottolog information by language.
#' @examples
#' myglotto <- add_glottolog(data = mydata, by = "language")
#' @export
#' @importFrom dplyr "%>%"
add_glottolog <- function(data, by="language"){

    ## warning for language/glottocode columns
    if((by == "language" && (!("language" %in% colnames(data) ))))
        stop("there is no column called 'language' in the dataset")
    else if(by == "glottocode" && (!("glottocode" %in% colnames(data))))
        stop("there is no column called 'glottocode' in the dataset")

    ## merge
    out <- dplyr::left_join(data, glottolog, by = by) %>%        
        as.data.frame
    out[out == ""] <- NA

    ## warning for unmatched language names
    if(by=="language"){

        helper2 <- data$language
        helper3 <- glottolog$language
        
        for (i in 1:length(helper2)){
            if(!(helper2[i] %in% helper3))
                warning(paste(paste("'", helper2[i], "'", sep = ""),
                              "is not found in glottolog", sep = " "),
                        immediate. = TRUE)
        }

    }

    ## warning for unmatched glottocodes
    else if(by=="glottocode"){

        helper2 <- data$glottocode
        helper3 <- glottolog$glottocode
        
        for (i in 1:length(helper2)){
            if(!(helper2[i] %in% helper3))
                warning(paste(paste("'", helper2[i], "'", sep = ""),
                              "is not found in glottolog", sep = " "),
                        immediate. = TRUE)
        }

    }

 
    ## warning for bookkeeping
    for (i in 1:nrow(out)){
        if(out[i, "bookkeeping"] != FALSE & !is.na(out[i, "bookkeeping"]))
            warning(
                paste(paste("'", out[i, "language"], "'", sep = ""),
                      "is no longer used in glottolog", sep = " "),
                immediate. = TRUE)
    }

    ## warning for dialect
    for (i in 1:nrow(out)){
        if(out[i, "level"] == "dialect" & !is.na(out[i, "level"]))
            warning(
                paste(paste("'", out[i, "language"], "'", sep = ""),
                      "is a dialect; replace by a language for geo data", sep = " "),
                immediate. = TRUE)
    }

    ## warning for family
    for (i in 1:nrow(out)){
        if(out[i, "level"] == "family" & !is.na(out[i, "level"]))
            warning(
                paste(paste("'", out[i, "language"], "'", sep = ""),
                      "is a family; replace by a language for geo data", sep = " "),
                immediate. = TRUE)
    }

    out
}

