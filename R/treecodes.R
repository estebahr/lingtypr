#' @title treecodes
#' @docType data
#' @usage data(treecodes)
#' @format An object of class \code{vector}.
#' \describe{
#' A vector with glottocodes.
#' }
#' @keywords datasets
#' @examples data(treecodes)
#' head(treecodes)
"treecodes"
