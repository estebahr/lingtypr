#' @title A string of glottocodes 
#' @docType data
#' @usage data(codes)
#' @format An object of class \code{"vector"}.
#' \describe{}
#' @references
#' @keywords datasets
#' @examples data(codes)
#' head(codes)
"codes"
