#' @title make_map
#' @description Plots a map of a language sample using latitude and longitude information to represent their location.
#' @param data A dataframe with a minimum column of languages ("language"), longitude ("longitude") and latitude ("latitude"). If macroarea=TRUE, a column with macroareas ("macroarea") is required. 
#' @param macroarea Option to color languages by macroarea. Defaults to FALSE.
#' @param labels Option to plot language names using labels. Defaults to FALSE.
#' @param repel If labels = TRUE, option to repel language names for better visibility.
#' @param feature Option to add the values of another feature ("myfeature") to the plot which will be represented by different colors. The feature can have categorical or numeric values.
#' @param legend Option to plot a legend below the map. Defaults to FALSE. 
#' @return Returns a ggplot object.
#' @examples 
#' mymap <- make_map(data = mymapdata, macroarea= TRUE, legend = TRUE)
#' unimap <- make_map(data = unimorphmap, macroarea = TRUE) 
#' @export
#' @importFrom dplyr "%>%"
make_map <- function(data,
                    macroarea=FALSE,
                    labels=FALSE,
                    repel=FALSE,
                    feature=NULL,
                    ## colors=FALSE,
                    ## shapes=FALSE,
                    legend=FALSE
                    ){

    longitude <- NULL
    latitude <- NULL
    language <- NULL
    long <- NULL
    lat <- NULL
    group <- NULL
    
    
    gamme_label <- c("#b3e6cc",
                    "#ffffcc",
                    "#ffdab3",
                    "#ffcccc",
                    "#ccffee",
                    "#c2d6d6")

    gamme_point <- c("#669900",
                    "#ffff4d",
                    "#ffa94d",
                    "#b30000",
                    "#66e0ff",
                    "#99ff66"
                    )

    xlims = c(-30, 330) 
    ylims = c(-80,80)

    ## warning for missing columns

    if(isFALSE(is.data.frame(data))){
        stop("'data' needs to be a dataframe")
    }
    
    else if(isTRUE(labels) & !("language" %in% colnames(data))){
        stop("could not find the column 'language'")
    }

    else if(isTRUE(macroarea) & !("macroarea" %in% colnames(data))){
        stop("could not find the column 'macroarea'")
    }

    else if(!("latitude" %in% colnames(data))){
        stop("could not find the column 'latitude'")
    }
    
    else if(!("longitude" %in% colnames(data))){
        stop("could not find the column 'longitude'")
    }


    ## warning for macroarea + FEATURE
    else if(isTRUE(macroarea) & !is.null(feature)){
        stop("specify 'macroarea=FALSE' to color an additional feature")
    }


    ## macroareas + labels
    else if(isTRUE(macroarea) & isTRUE(labels) & isFALSE(repel) & isTRUE(is.null(feature))
            ) {

        
        layer_labels <- ggplot2::geom_label(data = data,
                                           ggplot2::aes(x = longitude,
                                                        y = latitude,
                                                        label = language,
                                                        fill = macroarea),
                                           label.size = 0.5,
                                           color = "black")
        layer_points <- NULL

        layer_colors <- ggplot2::scale_fill_manual(values = gamme_label)

        layer_guides <- ggplot2::guides(fill = ggplot2::guide_legend(override.aes = list(color = NA),
                                                                    nrow = 1))  
    }

    ## macroareas + labels + repel
    else if(isTRUE(macroarea) & isTRUE(labels) & isTRUE(repel) & isTRUE(is.null(feature)) 
            ) {

        layer_labels <- ggrepel::geom_label_repel(data = data,
                                                 ggplot2::aes(x = longitude,
                                                              y = latitude,
                                                              label = language,
                                                              fill = macroarea),
                                                 label.size = 0.5,
                                                 box.padding = 0.5,
                                                 segment.size = 0.5,
                                                 max.overlaps = Inf,
                                                 color = "black",
                                                 show.legend  = FALSE)
        layer_points <- NULL

        layer_colors <- ggplot2::scale_fill_manual(values = gamme_label)

        layer_guides <- ggplot2::guides(fill = ggplot2::guide_legend(override.aes = list(color = NA),
                                                                    nrow = 1))  

    }

    ## macrcoareas + points
    else if(isTRUE(macroarea) & isFALSE(labels) & isTRUE(is.null(feature)) 
            ) {

        
        layer_labels <- NULL

        layer_points <- ggplot2::geom_jitter(data = data,
                                            ggplot2::aes(x = longitude,
                                                         y = latitude,
                                                         fill = macroarea),
                                            shape = 21,
                                            color="black",
                                            size = 3,
                                            width = 5,
                                            height = 5)

        layer_colors <- ggplot2::scale_fill_manual(name = "macro areas",
                                                  values = gamme_point)

        layer_guides <- ggplot2::guides(fill = ggplot2::guide_legend(nrow = 1))
    }

    ## monocolor + labels 
    else if(isFALSE(macroarea) & isTRUE(labels) & isFALSE(repel) & isTRUE(is.null(feature)) 
            ){
        
        layer_labels <- ggplot2::geom_label(data = data,
                                           ggplot2::aes(x = longitude,
                                                        y = latitude,
                                                        label = language,
                                                        fill = macroarea),
                                           label.size = 0.5,
                                           color = "black",
                                           show.legend  = FALSE)
        layer_points <- NULL
        
        layer_colors <- ggplot2::scale_fill_manual(values = c(rep("#80ffff",6)))
        
        layer_guides <- NULL 

    }

    ## monocolor + labels + repel
    else if(isFALSE(macroarea) & isTRUE(labels) & isTRUE(repel) & isTRUE(is.null(feature)) 
            ) {
        
        layer_labels <- ggrepel::geom_label_repel(data = data,
                                                 ggplot2::aes(x = longitude,
                                                              y = latitude,
                                                              label = language,
                                                              fill = macroarea),
                                                 label.size = 0.5,
                                                 box.padding = 0.5,
                                                 segment.size = 0.5,
                                                 max.overlaps = Inf,
                                                 color = "black",
                                                 show.legend  = FALSE)
        layer_points <- NULL

        layer_colors <- ggplot2::scale_fill_manual(values = c(rep("#80ffff",6)))

        layer_guides <- NULL
    }

    ## monocolor + points
    else if(isFALSE(macroarea) & isFALSE(labels) & isTRUE(is.null(feature)) 
            ) {
        
        layer_labels <- NULL

        layer_points <- ggplot2::geom_point(data = data,
                                           ggplot2::aes(x = longitude,
                                                        y = latitude,
                                                        fill = macroarea),
                                           shape = 21,
                                           color="black",
                                           size = 3,
                                           ## width = 5,
                                           ## height = 5,
                                           show.legend  = FALSE)
        
        layer_colors <- ggplot2::scale_fill_manual(values = c(rep("#00ffff",6)))

        layer_guides <- NULL
    }

    ## color by feature + labels
    if(isFALSE(macroarea) & isTRUE(labels) & isFALSE(repel) & isTRUE(!is.null(feature))){

        if(isTRUE(is.character(data[, feature])) | is.factor(data[, feature])){
            nvalues <- as.factor(data[, feature])
            nvalues <- levels(nvalues) %>%
                length()

            feat_fill <- viridisLite::viridis(nvalues)
            layer_colors <- ggplot2::scale_fill_manual(values = feat_fill,
                                                      name = feature)

            layer_guides <- ggplot2::guides(fill = ggplot2::guide_legend(override.aes = list(color = NA),
                                                                        nrow = 1))  
        }
        else{
            title <- feature
            layer_colors <- viridis::scale_fill_viridis(discrete = FALSE,
                                                       name = feature)

            layer_guides <- NULL

        }
            
        layer_labels <- ggplot2::geom_label(data = data,
                                           ggplot2::aes(x = longitude,
                                                        y = latitude,
                                                        label = language,
                                                        fill = data[, feature]),
                                           label.size = 0.5,
                                           color = "black")
        layer_points <- NULL


    }

    ##  + color by feature + labels + repel 
    if(isFALSE(macroarea) & isTRUE(labels) & isTRUE(repel) & isTRUE(!is.null(feature))){

        if(isTRUE(is.character(data[, feature]))){
            nvalues <- as.factor(data[, feature])
            nvalues <- levels(nvalues) %>%
                length()

            feat_fill <- viridisLite::viridis(nvalues)
            layer_colors <- ggplot2::scale_fill_manual(values = feat_fill,
                                                      name = feature)

            layer_guides <- ggplot2::guides(fill = ggplot2::guide_legend(override.aes = list(color = NA),
                                                                        nrow = 1))  

        }
        else{
            layer_colors <- viridis::scale_fill_viridis(discrete = FALSE,
                                                       name = feature)

            layer_guides <- NULL
        }
        
        layer_labels <- ggrepel::geom_label_repel(data = data,
                                                 ggplot2::aes(x = longitude,
                                                              y = latitude,
                                                              label = language,
                                                              fill = data[, feature]),
                                                 label.size = 0.5,
                                                 box.padding = 0.5,
                                                 segment.size = 0.5,
                                                 max.overlaps = Inf,
                                                 color = "black")
        layer_points <- NULL

        
    }

    ## color by feature + point 
    if(isFALSE(macroarea) & isFALSE(labels) & isTRUE(!is.null(feature))){

        if(!(feature %in% colnames(data))){
            stop(paste("could not find the column", paste("'", feature, "'", sep = ""), sep = " "))
        }

        
        layer_points <- ggplot2::geom_jitter(data = data,
                                            ggplot2::aes(x = longitude,
                                                         y = latitude,
                                                         fill = data[, feature]),
                                            shape = 21,
                                            color="black",
                                            size = 3,
                                            width = 5,
                                            height = 5)
        

        if(isTRUE(is.character(data[, feature]))){

            nvalues <- as.factor(data[, feature])
            nvalues <- levels(nvalues) %>%
                length()
            
            feat_fill <- viridisLite::viridis(nvalues)

            layer_colors <- ggplot2::scale_fill_manual(values = feat_fill,
                                                      name = feature)
            layer_guides <- ggplot2::guides(fill = ggplot2::guide_legend(nrow = 1))

        }
        else{
            layer_colors <- viridis::scale_fill_viridis(discrete = FALSE,
                                                       name = feature)

            layer_guides <- NULL
                }
        
        layer_labels <- NULL
        
    }

    
    if(isTRUE(legend)){
        layer_legend <- ggplot2::theme(legend.position = "bottom")
    }
    else{
        layer_legend <- ggplot2::theme(legend.position = "none")
    }

    p <- ggplot2::ggplot() +
        ggplot2::geom_polygon(data = world,
                              ggplot2::aes(x=long,
                                           y = lat,
                                           group = group),
                              color = "#595959",## "#8c8c8c",
                              fill = "#262626"
                              ) +
        ggplot2::coord_map(
                     "rectangular",
                     xlim=xlims,
                     ylim=ylims,
                     expand = TRUE) +
        layer_labels +
        layer_points +
        layer_colors +
        ggplot2::theme(panel.background = ggplot2::element_rect(fill = "#f2f2f2")) +
        ggplot2::xlab("longitude") +
        ggplot2::ylab("latitude") +
        ggplot2::scale_y_continuous(breaks = c(-80, -40, 0, 40, 80),
                                    labels = c("-80", "-40", "0", "40", "80")) +
        ggplot2::scale_x_continuous(breaks =
                                        c(-20,20,60,100,140,180,220, 260, 300),
                                    labels = c("-20", "20", "60",  "100","140", "180", "-140", "-100", "-60")
                           ) +
        layer_guides +
        layer_legend 



    p
    graphics::plot(p)

}
