#' @title add_wals
#' @description adds WALS data to a language sample
#' @param data a dataframe containing at least a column with languages (named 'language'), with glottocodes (named 'glottocode') or with wals codes (called 'wals_code')
#' @param by Option to match by language names ('language'), glottocodes ('glottocode') or wals codes ('wals_code'). Defaults to 'language'. 
#' @return Returns a dataframe with added WALS information by language.
#' @examples 
#' mywals <- add_wals(data = mydata, by = "language")
#' @export
#' @importFrom dplyr "%>%"
add_wals <- function(data, by = "language"){

    ## warning for language/glottocode columns
    if((by == "language" && (!("language" %in% colnames(data) ))))
        stop("there is no column called 'language' in data")
    if(by == "glottocode" && (!("glottocode" %in% colnames(data))))
        stop("there is no column called 'glottocode' in data")
    if(by == "wals_code" && (!("wals_code" %in% colnames(data))))
        stop("there is no column called 'wals_code' in data")

    
    ## warning for unmatched language names

    if(by=="language"){

        helper2 <- data$language
        helper3 <- wals$language
        
        for (i in 1:length(helper2)){
            if(!(helper2[i] %in% helper3))
                warning(paste(paste("'", helper2[i], "'", sep = ""),
                              "is not found in WALS", sep = " "),
                        immediate. = TRUE)
        }

    }

    ## warning for unmatched glottocodes
    if(by=="glottocode"){

        helper2 <- data$glottocode
        helper3 <- wals$glottocode
        
        for (i in 1:length(helper2)){
            if(!(helper2[i] %in% helper3))
                warning(paste(paste("'", helper2[i], "'", sep = ""),
                              "is not found in WALS", sep = " "),
                        immediate. = TRUE)
        }

    }


    ## warning for unmatched wals codes
    if(by=="wals_code"){

        helper2 <- data$wals_code
        helper3 <- wals$wals_code
        
        for (i in 1:length(helper2)){
            if(!(helper2[i] %in% helper3))
                warning(paste(paste("'", helper2[i], "'", sep = ""),
                              "is not found in WALS", sep = " "),
                        immediate. = TRUE)
        }

    }


    helper <- dplyr::left_join(data, wals) %>%
        as.data.frame

    helper

}
