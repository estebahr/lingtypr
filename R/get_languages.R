#' @title get_languages
#' @description Converts glottocodes into the respective language names.
#' @param data A vector or a dataframe containing glottocodes.
#' @param replace Option to replace glottocodes by language names. Defaults to FALSE.
#' @return Returns a vector or a dataframe with language names.
#' @examples 
#' mylanguages  <- get_languages(data = codes, replace = TRUE)
#' @export
#' @importFrom dplyr "%>%"
get_languages <- function(data, replace = FALSE){

    language <- NULL
    glottocode <- NULL
    
    ## for strings
    if(isTRUE(is.vector(data))){

        ## check for nonexisting language
        for(i in data){

            if(isFALSE(i %in% glottolog$glottocode)){
                stop(paste(
                    paste("'", i, "'", sep = ""), "is not found in glottolog", sep = " "))
            }
            
        }

        
        if(replace==TRUE){
            helper1 <- glottolog %>%
                dplyr::filter(glottocode %in% data) %>%
                dplyr::pull(language)
            helper1
        }
        else{
            helper1 <- glottolog %>%
                dplyr::filter(glottocode %in% data) %>%
                dplyr::pull(language)
            helper2 <- data.frame(glottocode = data,
                                 language = helper1)
            helper2
        }
    }

    ## for dataframe
    else if(isTRUE(is.data.frame(data))){

        ## check for language column
        if(!("glottocode" %in% colnames(data))){
            stop("there is no column called 'glottocode'")
        }

        ## check for nonexisting glottocode
        for(i in data$glottocode){

            if(isFALSE(i %in% glottolog$glottocode)){
                stop(paste(
                    paste("'", i, "'", sep = ""), "is not found in glottolog", sep = " "))
            }
            
        }

        if(replace==TRUE){

            helper1 <- glottolog %>%
                dplyr::select(language, glottocode) %>%
                as.data.frame
            helper2 <- dplyr::left_join(data, helper1, by = "glottocode") %>%
                dplyr::select(-c(glottocode)) %>%
                as.data.frame
            helper2

            ## helper1 <- dplyr::left_join(data, glottolog, by = "glottocode")
            ## helper1 <- dplyr::select(helper1, -c(helper1$family_id,
            ##                                     helper1$parent_id,
            ##                                     helper1$glottocode,
            ##                                     helper1$bookkeeping,
            ##                                     helper1$level,
            ##                                     helper1$child_family_count,
            ##                                     helper1$child_language_count,
            ##                                     helper1$child_dialect_count,
            ##                                     helper1$isocodes,
            ##                                     helper1$macroarea,
            ##                                     helper1$latitude,
            ##                                     helper1$longitude,
            ##                                     helper1$country_ids)) %>%
            ##     as.data.frame
            ## helper1
        }
        else{

            helper1 <- glottolog %>%
                dplyr::select(language, glottocode) %>%
                as.data.frame
            helper2 <- dplyr::left_join(data, helper1, by = "glottocode") %>%
                as.data.frame
            helper2

            ## helper1 <- dplyr::left_join(data, glottolog, by = "glottocode")
            ## helper1 <- dplyr::select(helper1, -c(helper1$family_id,
            ##                                     helper1$parent_id,
            ##                                     helper1$bookkeeping,
            ##                                     helper1$level,
            ##                                     helper1$child_family_count,
            ##                                     helper1$child_language_count,
            ##                                     helper1$child_dialect_count,
            ##                                     helper1$isocodes,
            ##                                     helper1$macroarea,
            ##                                     helper1$latitude,
            ##                                     helper1$country_ids,
            ##                                     helper1$longitude)) %>%
            ##     as.data.frame
            ## helper1
        }
    }
    else{
        warning("please provide a vector or dataframe")
    }

}
