#' @title get_heads
#' @description extracts heads of words in a UD treebank
#' @param data A dataframe of a treebank in the UD format.
#' @param feature A feature expressed as a string ('column') used to subset the data and select the words of interest. 
#' @param value A value expressed as a string ('value') present in 'feature' used to subset the data and select the words of interest.
#' @return A dataframe with added UD information of the heads of words (prefixed by 'h_').
#' @examples
#' myheads <- get_heads(data = udtest, feature = "upos", value = "ADV")
#' @export
get_heads <- function(data, feature, value){

    sentence_id = NULL
    token_id = NULL
    token = NULL
    upos = NULL
    xpos = NULL
    feats = NULL
    head_token_id = NULL
    dep_rel = NULL
    deps = NULL
    h_token_id = NULL
    

    ## make list from dataframe
    sentence.list <- split(data, data$sentence_id)
    ## make items 
    items <- lapply(sentence.list, function(x){
        item <- x[x[feature]==value, ]
        item    
    })
    items2 <- do.call("rbind", items)
    heads <- lapply(sentence.list, function(z){
        head_pos <- z[z[feature]==value, "head_token_id"]
        head <- z[z$token_id %in% head_pos, ] %>% 
            dplyr::select(sentence_id, token_id, token, upos, xpos, feats, head_token_id, dep_rel, deps) %>%
            dplyr::rename(h_token_id = token_id,
                          h_token = token,
                          h_upos = upos,
                          h_xpos = xpos,
                          h_feats = feats,
                          h_head_token_id = head_token_id,
                          h_dep_rel = dep_rel,
                          h_deps = deps
                          ) %>%
            as.data.frame 
        head
    })
    heads2 <- do.call("rbind", heads)
    merged <- dplyr::left_join(items2, heads2, by = "sentence_id") %>% 
        tidyr::drop_na(token_id, head_token_id) %>%
        dplyr::filter(head_token_id==h_token_id) %>% 
        as.data.frame
    merged
}

## feature <- "upos"
## value <- "ADV"
## data <- de
## head(data)
## sentence.list[[2]]
## items[[2]]
## head(items2)
## heads[[2]]
## head(heads2)
## str(merged)



## str(de)

## deoutput <- get_heads(data = de, feature = "upos", value = "ADV")

## write_csv(deoutput, "deoutput.csv")

## udtest <- de
## str(udtest)

## save(udtest, file = "udtest.RData", version = 2)

## head(test)
## str(test)


## sentence.list <- split(de, de$sentence_id)
## sentence.list[[2]]



## items <- lapply(sentence.list, function(x){
##     item <- x[x[feature]==value, ]
##     item    
## }
## )

## items[[2]]

## items2 <- do.call("rbind", items)
## head(items2)


## heads <- lapply(sentence.list, function(z){
##     head_pos <- z[z[feature]==value, "head_token_id"]

##     head <- z[z$token_id %in% head_pos, ]

##     head <- head %>%
##         select(sentence_id, token_id, token, upos, xpos, feats, head_token_id, dep_rel, deps) %>%
##         rename(h_token_id = token_id,
##                h_token = token,
##                h_upos = upos,
##                h_xpos = xpos,
##                h_feats = feats,
##                h_head_token_id = head_token_id,
##                h_dep_rel = dep_rel,
##                h_deps = deps
##                ) %>%
##         as.data.frame 
##     head
## })

## heads[[2]]

## heads2 <- do.call("rbind", heads)
## head(heads2)

## merged <- left_join(items2, heads2, by = "sentence_id")

## head(merged)
## nrow(merged)

## merged2 <- merged %>%
##     tidyr::drop_na(token_id, head_token_id) %>%
##     as.data.frame

## head(merged2)
## nrow(merged2)
