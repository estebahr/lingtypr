#' @title Relevant data extracted from glottolog  
#' @docType data
#' @usage data(glottolog)
#' @format An object of class \code{"dataframe"}.
#' \describe{
#' \item{language}{(unambiguous) language name as used in glottolog}
#' \item{glottocode}{glottocode}
#' \item{iso6393}{ISO code of the language}
#' \item{level}{level of the variety: dialect, language, family}
#' \item{parent_id}{glottocode of the parent language}
#' \item{family_id}{glottocode of the language family}
#' \item{child_family_count}{number of child families}
#' \item{child_language_count}{number of child languages}
#' \item{child_dialect_count}{number of child dialects}
#' \item{macroarea}{macroarea the language is spoken in}
#' \item{latitude}{latitude of the language location}
#' \item{longitude}{longitude of the language location}
#' \item{country_ids}{IDs of the countries the language is spoken in}
#' \item{bookkeeping}{whether or not the language is no longer used in glottolog}
#' }
#' @references Hammarström, Harald & Forkel, Robert & Haspelmath, Martin & Bank, Sebastian. 2021. Glottolog 4.4. Leipzig: Max Planck Institute for Evolutionary Anthropology. https://doi.org/10.5281/zenodo.4761960 (Available online at http://glottolog.org, Accessed on 2021-11-11.)
#' @source \url{https://doi.org/10.5281/zenodo.4761960}
#' @keywords datasets
#' @examples data(glottolog)
#' head(glottolog)
"glottolog"
