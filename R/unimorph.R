#' @title UniMorph dataset  
#' @docType data
#' @usage data(unimorph)
#' @format An object of class \code{"dataframe"}.
#' \describe{
#' \item{lexeme}{lexeme identifier}
#' \item{form}{form of the lexeme for a specific cell of the inflection paradigm}
#' \item{cell}{grammatical values of the cell of the inflection paradigm}
#' \item{iso6393}{iso6393 code of the language as provided in UniMorph (differs slightly from glottolog in certain cases)}
#' \item{language}{(unambiguous) language name as used in glottolog (language names used for UniMorph may differ slightly in certain cases)}
#' \item{glottocode}{glottocode of the language}
#' }
#' @references McCarthy, Arya D., Christo Kirov, Matteo Grella, Amrit Nidhi, Patrick Xia, Kyle Gorman, Ekaterina Vylomova, et al. “UniMorph 3.0: Universal Morphology.” In Proceedings of the 12th Language Resources and Evaluation Conference, 3922–31. Marseille, France: European Language Resources Association, 2020. https://aclanthology.org/2020.lrec-1.483.
#' @source \url{https://unimorph.github.io/}
#' @keywords datasets
#' @examples data(unimorph)
#' head(unimorph)
"unimorph"
