# lingtypR

The aim of the lingtypR package is to make useful datasets readily availabe in R and to provide functions that make common tasks easier and faster to code.


# Installation

```
## install.packages(devtools)
## devtools::install_git("git@gitlab.com:laurabecker/lingtypr.git")
## or
## install.packages(remotes)
## remotes::install_git("https://gitlab.com/laurabecker/lingtypr.git")
library(lingtypR)
```

# Usage

You find the description and examples of functions in this package [here](https://laurabecker.gitlab.io/html/lingtypR.html). 
