% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/make_map.R
\name{make_map}
\alias{make_map}
\title{make_map}
\usage{
make_map(
  data,
  macroarea = FALSE,
  labels = FALSE,
  repel = FALSE,
  feature = NULL,
  legend = FALSE
)
}
\arguments{
\item{data}{A dataframe with a minimum column of languages ("language"), longitude ("longitude") and latitude ("latitude"). If macroarea=TRUE, a column with macroareas ("macroarea") is required.}

\item{macroarea}{Option to color languages by macroarea. Defaults to FALSE.}

\item{labels}{Option to plot language names using labels. Defaults to FALSE.}

\item{repel}{If labels = TRUE, option to repel language names for better visibility.}

\item{feature}{Option to add the values of another feature ("myfeature") to the plot which will be represented by different colors. The feature can have categorical or numeric values.}

\item{legend}{Option to plot a legend below the map. Defaults to FALSE.}
}
\value{
Returns a ggplot object.
}
\description{
Plots a map of a language sample using latitude and longitude information to represent their location.
}
\examples{
mymap <- make_map(data = mymapdata, macroarea= TRUE, legend = TRUE)
unimap <- make_map(data = unimorphmap, macroarea = TRUE) 
}
